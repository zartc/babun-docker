#! /bin/bash

local BLUE='\033[1;34m'
local GREEN='\033[0;32m'
local NC='\033[0m'

# winpty base URL
babun_docker_winpty_base_url="https://github.com/rprichard/winpty/releases/download/0.4.3/"

# Specific file name, separated to allow unzipping it later
babun_docker_winpty_only_name="winpty-0.4.3-cygwin-2.8.0-ia32"

# Specific file extension
babun_docker_winpty_ext=".tar.gz"

# Complete filename
babun_docker_winpty_filename="$babun_docker_winpty_only_name$babun_docker_winpty_ext"

# Where to download winpty from
babun_docker_winpty_url="$babun_docker_winpty_base_url$babun_docker_winpty_filename"

# Directory in where to put Winpty
babun_docker_winpty_dir="$HOME/.local/winpty"


# babun-docker repo
babun_docker_repo="https://gitlab.com/zartc/babun-docker.git"

# Directory in where to put babun-docker
babun_docker_repo_dir="$HOME/.local/babun-docker"

# feedback format string
babun_docker_feedback="${BLUE}-- babun-docker:${NC}"


babun_docker_OLD_PWD=$(pwd)


# Set up winpty

# Create the directory to host winpty
if [[ ! -d $babun_docker_winpty_dir ]] ; then
   mkdir -p $babun_docker_winpty_dir ;
fi

# Enter the directory to download winpty
if [[ ! -f $babun_docker_winpty_dir/$babun_docker_winpty_filename ]] ; then
   # Remove old files
   rm -rf $babun_docker_winpty_dir
   mkdir -p $babun_docker_winpty_dir

   cd $babun_docker_winpty_dir

   # Download winpty
   wget -q --no-check-certificate $babun_docker_winpty_url -O $babun_docker_winpty_filename
   # Untar the downloaded file
   tar xf $babun_docker_winpty_filename
   # Move the tar contents to the current directory
   mv $babun_docker_winpty_only_name/bin/* ./
   # Remove untar-ed directory
   rm -rf $babun_docker_winpty_only_name
   # Make winpty executable
   chmod 777 $babun_docker_winpty_dir/*
   # Ask for update
   echo "$babun_docker_feedback to finish the installation please run: babun-docker-update"
fi

export PATH="$babun_docker_winpty_dir:$PATH"


# Set up babun-docker

# Create the directory to host babun-docker
if [[ ! -d $babun_docker_repo_dir ]] ; then
   git clone $babun_docker_repo $babun_docker_repo_dir
   echo "$babun_docker_feedback to finish the installation please run: babun-docker-update"
fi

cd $babun_docker_repo_dir

source ./*config.sh
source ./bin/babun-docker.sh


# Set up setup on start for ZSH and Bash
babun_docker_setup_str="[ -s '$babun_docker_repo_dir/setup.sh' ] && source '$babun_docker_repo_dir/setup.sh'"

if [[ -f $HOME/.zshrc ]] && [[ -z $(grep "$babun_docker_repo_dir/setup.sh" $HOME/.zshrc) ]] ; then
  echo $babun_docker_setup_str >> $HOME/.zshrc
fi
if [[ -f $HOME/.bashrc ]] && [[ -z $(grep "$babun_docker_repo_dir/setup.sh" $HOME/.bashrc) ]] ; then
  echo $babun_docker_setup_str >> $HOME/.bashrc
fi

cd $babun_docker_OLD_PWD

# Setup update function
function babun-docker-update {
  local old_pwd=$(pwd)
  echo "$babun_docker_feedback Updating babun-docker"
  cd $babun_docker_repo_dir && git pull && source ./setup.sh
  cd $old_pwd
}
